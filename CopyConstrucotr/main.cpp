#include <QTextStream>
#include "fraction.h"

int main()
{
    QTextStream cout(stdout);

    Fraction twothrids(2,3);
    Fraction threequarters(3,4);
    Fraction acopy(twothrids);
    Fraction f4 = threequarters;//这个不是调用operator形式的拷贝构造函数？？

    qDebug() ;qDebug() ;qDebug() ;

    cout  << "after declaratons -" << Fraction::report();
    f4 = twothrids;//operato形式的拷贝构造函数
    cout << "\n before multiply - " << Fraction::report();
    f4 = twothrids.multiply(threequarters);//先return 一个对象，调用拷贝构造函数，再＝赋值，调用operator拷贝构造函数
    cout << "\nafter multiply - " << Fraction::report() << endl;

    exit(0);
}

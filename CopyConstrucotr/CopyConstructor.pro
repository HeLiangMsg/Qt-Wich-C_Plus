#-------------------------------------------------
#
# Project created by QtCreator 2016-11-12T22:05:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = CopyConstructor
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    fraction.cpp

HEADERS += \
    fraction.h

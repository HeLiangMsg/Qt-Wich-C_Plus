#include "fraction.h"

//初始化静态类成员变量
int Fraction::s_assigns = 0;
int Fraction::s_copies = 0;
int Fraction::s_ctors = 0;

   //每一个类成员变量首先都包含默认的初始化值，然后被赋予一个值。这样做不会有错误的情况。但是多一次的初始化会浪费处理时间。相反，如果直接在构造函数的初始化列表中对成员变量进行初始化则会省去第二赋值的时间。

Fraction::Fraction(int a, int d):
    m_Number(a),m_Denom(d)
{
    Q_ASSERT(m_Denom !=0);
    ++s_ctors;

    qDebug("Constrcation");
}

//拷贝构造函数
Fraction::Fraction(const Fraction &other)
    :m_Number(other.m_Number),m_Denom(other.m_Denom)
{
    ++s_copies;
    qDebug("Copy Constraction");
}

Fraction& Fraction::operator=(const Fraction& other)
{
    if (this != &other) {
        m_Denom = other.m_Denom;
        m_Number = other.m_Number;
        ++s_assigns;
    }
    qDebug("Operator= Constraction");
    return *this;
}

Fraction Fraction::multiply(Fraction f2)
{
    return Fraction(m_Number*f2.m_Number,m_Denom*f2.m_Denom);
}

QString Fraction::report()
{
    return QString("[assigns: %1,copies: %2, ctors: %3]").arg(s_assigns).arg(s_copies).arg(s_ctors);
}
